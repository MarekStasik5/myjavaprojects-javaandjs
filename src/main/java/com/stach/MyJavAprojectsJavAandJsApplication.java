package com.stach;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyJavAprojectsJavAandJsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyJavAprojectsJavAandJsApplication.class, args);
	}

}

