angular.module('app', ['ngResource'])
.controller('ProjectController', function($http, $resource) {
	var vm = this;
	var Project = $resource('api/projects/:projectId');
	vm.project = new Project();
	
	function refreshData() {
		vm.projects = Project.query(
				function success(data, headers) {
					console.log('Loaded data: ' +  data);
					console.log(headers('Content-Type'));
				},
				function error(reponse) {
					console.log(response.status);
				});
	}

	vm.addProject = function(project) {
		console.log(vm.project.__proto__);
		vm.project.$save(function(data) {
			refreshData();
			vm.project = new Project();
		});
	}
	
	vm.loadData = function(id) {
		vm.details = Project.get({projectId: id});
	}
	
	vm.appName = 'Project List';
	refreshData();
});