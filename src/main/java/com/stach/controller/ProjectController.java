package com.stach.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.stach.model.Project;
import com.stach.repository.ProjectReposiotry;

@RestController
@RequestMapping("/api/projects")
public class ProjectController {

	private ProjectReposiotry projectRepository;

	@Autowired
	public ProjectController(ProjectReposiotry projectRepository) {
		this.projectRepository = projectRepository;
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Project>> allProjects() {
		List<Project> allProjects = projectRepository.findAll();
		return ResponseEntity.ok(allProjects);
	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Optional<Project>> getProjectById(@PathVariable Long id) {
		Optional<Project> project = projectRepository.findById(id);
		return ResponseEntity.ok(project);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveProject(@RequestBody Project project) {
		Project projectSave = projectRepository.save(project);
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(projectSave.getId())
				.toUri();
		return ResponseEntity.created(location).body(projectSave);
	}
}
